FROM debian:12-slim
LABEL maintainer="Ops One AG <team@opsone.ch>"

ENV DEBIAN_FRONTEND=noninteractive
ENV LANG=C.UTF-8

RUN apt-get update \
    && apt-get install --yes --no-install-recommends git librrds-perl libjson-perl libhtml-parser-perl python3 python3-dotenv libcgi-pm-perl perl-modules-5.36 ca-certificates \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

RUN mkdir -p /etc/collectd \
    && echo 'datadir: "/tmp/rrd"' > /etc/collectd/collection.conf

RUN git clone --depth 1 https://github.com/httpdss/collectd-web.git /usr/local/collectd-web/

WORKDIR /usr/local/collectd-web/

EXPOSE 80

CMD ["python3", "runserver.py", "0.0.0.0", "80"]
